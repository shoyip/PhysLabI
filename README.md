# Physics Lab I (Prodi)

In this repository I uploaded the data and python scripts for the five Lab
experiments of the Physics Lab I course we undertook. Here they are, in order:

- **Experiment 1**: reapeted measures of the length of samples from a population
  of metallic cilinders and of the period of a pendulum (grade C);
- **Experiment 2**: static and dynamic study of spring deformation (grade B);
- **Experiment 3**: study of the oscillation of the simple gravity pendulum and
  measure of gravitational acceleration;
- **Experiment 4**: study of the isochoric transformation of an ideal gas;
- **Experiment 5**: random walks and monte carlo simulations.

At this stage (Wednesday, 20 June 2018) the first three reports are done, the
fourth is a Work in Progress and the fifth is pending. Delivered reports are
named "relazione" and corrected reports are named "correzione".

Data is stored in `.csv` files and scripts are in written in Python (first
written in Jupyter Notebooks, then converted to python scripts) except for the
first experiment, in which we as a group used MATLAB.

Feel free to use the files contained in these folders, except for the pdf files
explaining the experiments, which were written by Professors [P. Fornasini][1] and
[G.A. Prodi][2] from the University of Trento.

Valeria Facchinelli, Andrea Pegoretti and Shoichi Yip (me) are authors of all
the code and reports contained in these directories.

[1]: https://webapps.unitn.it/du/it/Persona/PER0004343
[2]: https://webapps.unitn.it/du/it/Persona/PER0003102

## References

- Fornasini, P. (2008). The uncertainty in physical measurements: an
  introduction to data analysis in the physics laboratory. Springer-Verlag New
York
- Loreti, M. (2006). Teoria degli errori e fondamenti di statistica. Decibel,
  Zanichelli.
