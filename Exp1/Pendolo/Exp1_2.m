close all;
units = 's';
cd /home/shoichi/Documenti/Uni/Materiale/Prodi/Relazioni/Exp1/

cd Pendolo

%% Queste sono le funzioni che importano i dati.
[t1_1,t2,t3] = importfile1('misure_periodo.csv',2, 21);
t1_2 = importfile2('pendolo_pego.csv',2, 81);
t4 = importfile('pendolo_resm.csv',2, 20);

close all;
%% ???
t0_pego = t1_1;
t0_vale = t2;
t0_sho = t3;

%% Trovo il miglior operatore, calolo media, e deviazione standard dei dati presi da ogni componente del gruppo
media1 = mean(t1_1);
media2 = mean(t2);
media3 = mean(t3);

media1
media2
media3

%% Calcolo della deviazione standard di ogni operatore
devstd1 = std(t1_1);
devstd2 = std(t2);
devstd3 = std(t3);

devstd1
devstd2
devstd3

%% Definisco il binnaggio degli istogrammi delle prime venti misure
min_pego = min(t1_1);
min_sho = min(t3);
min_vale = min(t2);

dxCron = 0.01;

max_pego = max(t1_1);
max_sho = max(t3);
max_vale = max(t2);

dpego = max_pego - min_pego
dvale = max_vale - min_vale
dsho = max_sho - min_sho

m_bin = sqrt(20);

step_pego = dpego / 4
step_vale = dvale / 4
step_sho = dsho / 4

%% Cinque periodi in una misura
p2_1 = t4 ./ 5
p2_2 = [p2_1; p2_1; p2_1; p2_1; p2_1];

%% Sovrappongo gli istogrammi di ogni per stabilire quale dei tre componenti dovr'à eseguire le misure 
fig1 = figure()
hold on
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);
xl = xlabel(['T [' units ']']);
set(xl, 'FontSize', 24);
grid on;
histT1_1 = histogram(t1_1,4,'FaceColor','r','Normalization','pdf')
histT2 = histogram(t2,4,'FaceColor','b','Normalization','pdf')
histT3 = histogram(t3,4,'FaceColor','g','Normalization','pdf')
hleg = legend([histT1_1, histT2, histT3], 'Pegoretti', 'Facchinelli', 'Yip');
matlab2tikz('hist11.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% Calclolo la media e la deviazione standard delle 100 misure complessive 
pegofin = [t1_1; t1_2];

avg_pegofin = mean(pegofin);
stddev_pegofin = std(pegofin)

fprintf('La misura di Pego è: %d con errore %d.\n', avg_pegofin, stddev_pegofin);
%% Binnagio dell'istogramma con le 100 misure complessive
pego_nbin = sqrt(numel(pegofin));
pego_max = max(pegofin);
pego_min = min(pegofin);
pego_dfin = pego_max - pego_min;
pego_stepfin = pego_dfin / pego_nbin;

stdPegoFin = std(pegofin);

fig2 = figure()
hold on
yl = ylabel('f^*_j');
set(yl, 'FontSize', 24);
xl = xlabel(['T [' units ']']);
set(xl, 'FontSize', 24);
grid on;
histPego = histogram(t1_1,4,'Normalization','pdf')
histPegoFin = histogram(pegofin,pego_nbin,'Normalization','pdf');
hleg = legend([histPego, histPegoFin], 'Pegoretti 20', 'Pegoretti 100')
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
matlab2tikz('hist12.tex','height','\figureheight', 'width','\figurewidth');
grid on
hold off

%% Istogramma 20x5
units = 's';
minT4 = min(t4);
maxT4 = max(t4);
dxT4 = 0.002;

nStepT4 = sqrt(numel(t4));
kStepT4 = round(((maxT4 - minT4)/nStepT4)/dxT4);
stepT4 = kStepT4 * dxT4;

minHistT4 = minT4 - stepT4/2;
maxHistT4 = maxT4 + stepT4/2;
dataHistT4 = [minHistT4 : stepT4 : maxHistT4];

nT4 = histcounts(t4, dataHistT4);

fig3 = figure();
histT4 = histogram(t4, dataHistT4, 'Normalization', 'probability');
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);
xl = xlabel(['T [' units ']']);
set(xl, 'FontSize', 24);
hleg = legend([histT4], 'Misure di 5 periodi')
matlab2tikz('hist13.tex','height','\figureheight', 'width','\figurewidth');
grid on;
hold off

%% Istogramma 10x10
mat10 = reshape(pegofin,[10,10]);
mean10 = [];
std10 = [];
mat10 =  mat10(randperm(end),:);
for ii=1:10
    A = mean(mat10(ii:10:100))
    mean10 = [mean10; A]
    B = std(mat10(ii:10:100))
    std10 = [std10; B]
end
minMean10 = min(mean10);
maxMean10 = max(mean10);
nStepMean10 = sqrt(numel(mean10));
kStepMean10 = round((maxMean10 - minMean10)/nStepMean10/dxCron)+.5;
stepMean10 = kStepMean10 * dxCron;

dataHistMean10 = [minMean10-stepMean10/2 : stepMean10 : maxMean10+stepMean10/2]
std10Tot = std(mean10)
stdPegoFin

fig4 = figure();
hold on
hist10 = histogram(mean10, dataHistMean10, 'Normalization', 'probability')
grid on
legend = 'off'
yl = ylabel(['p^*_j [1/' units ']']);
set(yl, 'FontSize', 24);
xl = xlabel(['T [' units ']']);
set(xl, 'FontSize', 24);
hleg = legend([hist10], 'Dati divisi in gruppi di 10')
matlab2tikz('hist14.tex','height','\figureheight', 'width','\figurewidth');
hold off