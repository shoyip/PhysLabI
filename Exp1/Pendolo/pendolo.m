close all;
units = 'mm';
cd /home/shoichi/Documenti/Uni/Materiale/Prodi/Relazioni/Exp1/

cd Pendolo

%% Queste sono le funzioni che importano i dati.
[pego,vale,sho] = importfile1('misure_periodo.csv',2, 21);
periodo = importfile2('pendolo_pego.csv',2, 81);

close all;
%%
t0_pego = pego;
t0_vale = vale;
t0_sho = sho;

%% Trovo il miglior operatore, calolo media, e deviazione standard dei dati presi da ogni componente del gruppo
media1 = mean(pego);
media2 = mean(vale);
media3 = mean(sho);

media1
media2
media3

%% Calcolo della deviazione standard di ogni operatore
devstd1 = std(pego);
devstd2 = std(vale);
devstd3 = std(sho);

devstd1
devstd2
devstd3

%% Definisco il binnaggio degli istogrammi delle prime venti misure
min_pego = min(pego);
min_sho = min(sho);
min_vale = min(vale);

max_pego = max(pego);
max_sho = max(sho);
max_vale = max(vale);

dpego = max_pego - min_pego
dvale = max_vale - min_vale
dsho = max_sho - min_sho

m_bin = sqrt(20);

step_pego = dpego / 4
step_vale = dvale / 4
step_sho = dsho / 4

%% Sovrappongo gli istogrammi di ogni per stabilire quale dei tre componenti dovr'à eseguire le misure 
fig1 = figure();
histogram(pego,4,'FaceColor','r','Normalization','pdf')
hold on
histogram(vale,4,'FaceColor','b','Normalization','pdf')
hold on
histogram(sho,4,'FaceColor','g','Normalization','pdf')
grid on

%% Calclolo la media e la deviazione standard delle 100 misure complessive 
pegofin = [pego; periodo];

avg_pegofin = mean(pegofin);
stddev_pegofin = std(pegofin);

fprintf('La misura di Pego è: %d con errore %d.\n', avg_pegofin, stddev_pegofin);
%% Binnagio dell'istogramma con le 100 misure complessive
pego_nbin = sqrt(numel(pegofin));
pego_max = max(pegofin);
pego_min = min(pegofin);
pego_dfin = pego_max - pego_min;
pego_stepfin = pego_dfin / pego_nbin;

fig2 = figure();
histogram(pego,4,'Normalization','pdf')
hold on
histogram(pegofin,pego_nbin,'Normalization','pdf');
grid on

