%% Report per il pendolo

% pego è 1, vale è 2, sho è 3
[t1_1,t2,t3] = importfile1('misure_periodo.csv',2, 21);
t1_2 = importfile2('pendolo_pego.csv',2, 81);
% t4 è la misura di cinque oscillazioni consecutive
t4 = importfile('pendolo_resm.csv',2, 21);

% calcolo i parametri statistici per ogni operatore
mt1_1 = mean(t1_1)
std1_1 = std(t1_1)
var1_1 = var(t1_1)
mt2 = mean(t2)
std2 = std(t2)
var2 = var(t2)
mt3 = mean(t3)
std3 = std(t3)
var3 = var(t3)

pegofin = [t1_1; t1_2];

% calcolo i parametri statistici per le 100 misure di pego
m100 = mean(pegofin)
std100 = std(pegofin)
var100 = var(pegofin)

% calcolo i parametri statistici per le 25 misure di 5 periodi
m5 = mean(t4)
std5 = std(t4)
var5 = var(t4)

% calcolo i parametri statistici per le 100 misure raggruppate in 10
mat10 = reshape(pegofin,[10,10]);

mean10 = [];
std10 = [];
mat10 =  mat10(:,randperm(size(mat10,2)));
mat10 =  mat10(randperm(size(mat10,1)),:)
for ii=1:10
    A = mean(mat10(ii:10:100));
    mean10 = [mean10; A];
    B = std(mat10(ii:10:100));
    std10 = [std10; B];
end
mean10_1 = mean(mean10)
std10_1 = std(mean10)
var10_1 = var(mean10)
r = std(mean10)/std(pegofin)

%C=randn(10);
%c = reshape(C,[1,100]);
%meanD = [];
%stdD = [];
%for ii=1:10
%    A1 = mean(C(ii:10:100));
%    meanD = [meanD; A1];
%    B1 = std(C(ii:10:100));
%    stdD = [stdD; B1];
%end
%r = std(c)/std(meanD)

r
mat10
std(pegofin)
mean(mat10)
std(mean(mat10))
std(mean(mat10))/std(pegofin)
csvwrite('pegofinmat.csv',mat10)
std(pegofin)/std(mean(mat10))