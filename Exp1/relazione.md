## Introduzione


L'esperienza consiste di due parti: la misura delle lunghezze di una
popolazione di campioni (cilindretti) prodotti in serie e la misura delle
oscillazioni di un pendolo semplice.


Nella prima parte prendiamo in considerazione una popolazione la cui lunghezza
nominale è fissata. Questi campioni in realtà sono diversi, e tali differenze
possono essere osservate solo attraverso strumenti con adeguata risoluzione.
Vogliamo inoltre confrontare la distribuzione della popolazione (dei
cilindretti) quando gli stessi campioni vegnono misurati attraverso strumenti
con diversa risoluzione.  Nella seconda parte vogliamo osservare come migliora
l'incertezza dovuta a errori di tipo A sullo stesso fenomeno (ipotesi di
isocronia del pendolo semplice) all'aumentare del numero di misure oppure
distribuendo l'errore su più misure. Ad esempio, volendo misurare lo spessore
di un foglio di carta, con uno strumento di risoluzione maggiore dello
spessore, possiamo misurare lo spessore di $N$ fogli sovrapposti e dividere la
misura per $N$; cosi facendo si diminuisce la risoluzione di misura dello
strumento di un fattore $N$.


## Procedure di misura


Per realizzare il pendolo semplice, appendiamo una massa a un supporto fisso
rispetto al laboratorio con un filo, che supponiamo inestensibile e di massa
trascurabile. Per soddisfare le ipotesi di isocronia del pendolo vogliamo
trovare un angolo inziale (rispetto alla verticale) piccolo. A tal fine
utilizziamo la relazione $\sin{\theta} = \theta$ per piccole oscillazioni, e
dalla definizione di radiante ($\theta = \frac{l}{R}$) troviamo la distanza
$l$, fissato $\theta$.  Riteniamo $\theta = 5°$ sufficentemente piccolo.
Selezioniamo il miglior operatore tra i componenti del gruppo confrontando i
rispettivi intervalli di incertezza su 20 misure con il valore atteso del
periodo del pendolo
\[
2 \pi \sqrt{\frac{l}{g}}.
\] 
