close all;
units = 'mm';
cd /home/shoichi/Documenti/Uni/Materiale/Prodi/Relazioni/Exp1/

cd Lunghezze

%% Importo il file dei dati
[Numero_campione,Misure_calibro,Misure_micrometro,Sottile] = importfile('misure1.csv',2, 26);

%% Misura del calibro: plotto l'istogramma delle misure del calibro
minCalib = min(Misure_calibro);
maxCalib = max(Misure_calibro);
dxCalib = 0.01;

% Lo step dell'istogramma deve essere proporzionale al dx
nStepCalib = sqrt(numel(Misure_calibro));
%nStepCalib = 3;
kStepCalib = round(((maxCalib - minCalib) / nStepCalib) / dxCalib);
stepCalib = kStepCalib * dxCalib;

% Imposto gli estremi dell'istogramma Calibro e i limiti dei bin
minHistCalib = minCalib - stepCalib / 2;
maxHistCalib = maxCalib + stepCalib / 2;
dataHistCalib = [ minHistCalib : stepCalib : maxHistCalib ];
%dataHistCalib = [ minMicr : stepCalib : maxMicr ];

% Calcolo i conteggi: creo un array con tutti i numeri di conteggi per
% ogni bin
nCalib = histcounts(Misure_calibro, dataHistCalib);
NCalib = sum(nCalib);

stDevCalib = std(Misure_calibro)

% Creo una figura, fig1
%fig1 = figure()
%hold on
%% ax = gca;
%xt4 = [ minHistCalib : stepCalib : maxHistCalib ]
%xticks('manual')
%xticks(xt4)
%title('Misura di 25 cilindretti con calibro')
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del Calibro
%histCalib = histogram(Misure_calibro, dataHistCalib,'Normalization','pdf');

% Converti i plot per latex
%matlab2tikz('hist1.tex','height','\figureheight', 'width','\figurewidth');
%% Misura del micrometro: plotto l'istogramma delle misure del micrometro
minMicr = min(Misure_micrometro);
maxMicr = max(Misure_micrometro);
dxMicr = 0.01;

% Lo step dell'istogramma deve essere proporzionale al dx
nStepMicr = sqrt(numel(Misure_micrometro));
%nStepMicr = 3;
kStepMicr = round(((maxMicr - minMicr) / nStepMicr) / dxMicr);
stepMicr = kStepMicr * dxMicr;

% Imposto gli estremi dell'istogramma micrometro e i limiti dei bin
minHistMicr = minMicr - stepMicr / 2;
maxHistMicr = maxMicr + stepMicr / 2;
dataHistMicr = [ minHistMicr : stepMicr : maxHistMicr ];

% Calcolo i conteggi: creo un array con tutti i numeri di conteggi per
% ogni bin
nMicr = histcounts(Misure_micrometro, dataHistMicr);
NMicr = sum(nMicr);

stDevMicr = std(Misure_micrometro)

% Creo una figura, fig1
%fig2 = figure()
%hold on
% ax = gca;
%xt1 = [ minHistMicr : stepMicr : maxHistMicr ]
%xticks('manual')
%xticks(xt1)
%legend('off')
%title('Misura di 25 cilindretti con micrometro')
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del micrometro
%histMicr = histogram(Misure_micrometro, dataHistMicr,'Normalization','pdf');

% Converti i plot per latex
%matlab2tikz('hist2.tex','height','\figureheight', 'width','\figurewidth');

%% Importo le misure di tutti i gruppi
% Importo i dati
[tot_no,tot_calib,tot_micr] = importfile1('dati.csv',1, 325);

%% Misure di calibro di 324 misure

minTotCalib = min(tot_calib);
maxTotCalib = max(tot_calib);
dxTotCalib = 0.05;

% Lo step dell'istogramma deve essere proporzionale al dx
nStepTotCalib = sqrt(numel(tot_calib));
kStepTotCalib = round(((maxTotCalib - minTotCalib) / nStepTotCalib) / dxTotCalib) + 1;
stepTotCalib = kStepTotCalib * dxTotCalib;

stepTotCalib1 = round(((maxTotCalib - minTotCalib) / nStepTotCalib) / dxTotCalib) * dxTotCalib;

% Imposto gli estremi dell'istogramma Calibro e i limiti dei bin
minHistTotCalib = minTotCalib - stepTotCalib / 2;
maxHistTotCalib = maxTotCalib + stepTotCalib / 2;
dataHistTotCalib = [ minTotCalib : stepTotCalib : maxTotCalib ];

dataHistTotCalib1 = [ minTotCalib : stepTotCalib1 : maxTotCalib ];

% Calcolo i conteggi: creo un array con tutti i numeri di conteggi per
% ogni bin
nTotCalib = histcounts(tot_calib, dataHistTotCalib);
NTotCalib = sum(nTotCalib)

nTotCalib1 = histcounts(tot_calib, dataHistTotCalib1);
NTotCalib1 = sum(nTotCalib1)

stDevTotCalib = std(tot_calib)

% Creo una figura, fig1
%fig3 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
%hold on
% ax = gca;
%xt3 = [ minHistTotCalib : stepTotCalib : maxHistTotCalib ]
%xticks('manual')
%xticks(xt3)
%legend('off')
%title('Misura di 324 cilindretti con calibro')
% hold on
% set(gca,'FontSize',10)

% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotCalibro
%histTotCalib = histogram(tot_calib, dataHistTotCalib,'Normalization','pdf');

% Converti i plot per latex
%matlab2tikz('hist3.tex','height','\figureheight', 'width','\figurewidth');
%% Misure di micrometro di 324 misure

minTotMicr = min(tot_micr);
maxTotMicr = max(tot_micr);
dxTotMicr = 0.01;

% Lo step dell'istogramma deve essere proporzionale al dx
nStepTotMicr = sqrt(numel(tot_micr));
kStepTotMicr = round(((maxTotMicr - minTotMicr) / nStepTotMicr) / dxTotMicr) + 1;
stepTotMicr = (kStepTotMicr) * dxTotMicr;

stepTotMicr1 = round(((maxTotMicr - minTotMicr) / nStepTotMicr) / dxTotMicr) * dxTotMicr;

% Imposto gli estremi dell'istogramma Micrro e i limiti dei bin
minHistTotMicr = minTotMicr - stepTotMicr / 2;
maxHistTotMicr = maxTotMicr + stepTotMicr / 2;
dataHistTotMicr = [ minTotMicr : stepTotMicr : maxTotMicr ];

dataHistTotMicr1 = [ minTotMicr : stepTotMicr1 : maxTotMicr ];

% Calcolo i conteggi: creo un array con tutti i numeri di conteggi per
% ogni bin
nTotMicr = histcounts(tot_micr, dataHistTotMicr);
NTotMicr = sum(nTotMicr);

nTotMicr1 = histcounts(tot_micr, dataHistTotMicr1);
NTotMicr1 = sum(nTotMicr1);

stDevTotMicr = std(tot_micr)

% Creo una figura, fig1
%fig4 = figure() % crea una nuova fig4
% figure(fig2) % sovrapponi a fig2
%hold on
% ax = gca;
%xt4 = [ minHistTotMicr : stepTotMicr : maxHistTotMicr ]
%xticks('manual')
%xticks(xt4)
%legend('off')
%title('Misura di 324 cilindretti con micrometro')
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotMicrro
%histTotMicr = histogram(tot_micr, dataHistTotMicr,'Normalization','pdf');

% Converti i plot per latex
%matlab2tikz('hist4.tex','height','\figureheight', 'width','\figurewidth');

%% Plotto il confronto tra 25 calibro e 25 micrometro

% Creo una figura, fig1
fig1 = figure()
hold on
% ax = gca;
xt1_1 = [ minCalib : stepCalib : maxCalib ]
xt1_2 = [ minMicr : stepMicr : maxMicr ]
xt1 = sort([xt1_1 xt1_2]);
xticks('manual')
xticks(xt1)
title('Misura di 25 cilindretti con calibro e con micrometro')
xticks('manual')
xticks(xt1_1)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on

% Disegno l'istogramma del Calibro
histCalib = histogram(Misure_calibro, dataHistCalib,'Normalization','pdf','FaceColor','b');

% Disegno l'istogramma del micrometro
histMicr = histogram(Misure_micrometro, dataHistMicr,'Normalization','pdf','FaceColor','g');

hleg = legend([histCalib, histMicr], 'Calibro','Micrometro');

% Converti i plot per latex
matlab2tikz('hist1.tex','height','\figureheight', 'width','\figurewidth');

hold off

%% Plotto i 324 calibro e i 324 micrometro

% Creo una figura, fig1
fig2 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt2_1 = [ minTotCalib : stepTotCalib : maxTotCalib ]
xt2_2 = [ minTotMicr : stepTotMicr : maxTotMicr ]
xt2 = sort([xt2_1 xt2_2]);
xticks('manual')
xticks(xt2_1)
legend('off')
title('Misura di 324 cilindretti con calibro e con micrometro')
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotCalibro
histTotCalib = histogram(tot_calib, dataHistTotCalib,'Normalization','pdf','FaceColor','r');

% Disegno l'istogramma del TotMicrro
histTotMicr = histogram(tot_micr, dataHistTotMicr,'Normalization','pdf','FaceColor','[1 0.6 0]');

hleg = legend([histTotCalib, histTotMicr], 'Calibro','Micrometro');

% Converti i plot per latex
matlab2tikz('hist2.tex','height','\bigfigureheight', 'width','\bigfigurewidth');
hold off

%% Plotto 25 calibro e 324 calibro

% Creo una figura, fig1
fig3 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt3_1 = [ minHistTotCalib : stepTotCalib : maxHistTotCalib ]
xt3_2 = [ minHistCalib : stepCalib : maxHistCalib ];
xticks('manual');
xt3 = sort([xt3_1 xt3_2]);
xticks(xt3_1)
legend('off')
title('Misura di 324 e 25 cilindretti con il calibro')
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del Calibro
histCalib = histogram(Misure_calibro, dataHistCalib,'Normalization','pdf','FaceColor','b');

% Disegno l'istogramma del TotCalibro
histTotCalib = histogram(tot_calib, dataHistTotCalib,'Normalization','pdf','FaceColor','r');

hleg = legend([histCalib, histTotCalib], '25 misure','324 misure');

% Converti i plot per latex
matlab2tikz('hist3.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% Plotto 25 micrometro e 324 micrometro

% Creo una figura, fig1
fig4 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt4_1 = [ minHistTotCalib : stepTotCalib : maxHistTotCalib ]
xt4_2 = [ minTotCalib : stepCalib : maxTotCalib ]
xt4 = sort([xt4_1 xt4_2]);
xticks('manual')
xticks(xt4_1)
legend('off')
title('Misura di 324 e 25 cilindretti con il micrometro')
yl = ylabel(['f^*_j [1/' units ']']);
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotMicrro
histTotMicr = histogram(tot_micr, dataHistTotMicr,'Normalization','pdf','FaceColor','[1 0.6 0]');

% Disegno l'istogramma del micrometro
histMicr = histogram(Misure_micrometro, dataHistMicr,'Normalization','pdf','FaceColor','g');

hleg = legend([histMicr, histTotMicr], '25 misure','324 misure');

% Converti i plot per latex
matlab2tikz('hist4.tex','height','\bigfigureheight', 'width','\bigfigurewidth');
hold off

%% calibro frequenza 25
fig5 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt5 = [ minTotCalib : stepCalib : maxTotCalib ]
xticks('manual')
xticks(xt5)
legend('off')
title('Misura di 25 cilindretti con calibro normalizzati per altezza')
yl = ylabel('p^*_j');
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del Calib
histCalib = histogram(tot_calib, dataHistCalib,'Normalization','probability','FaceColor','b');

hleg = legend([histCalib], 'Calibro');

% Converti i plot per latex
matlab2tikz('hist5.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% micrometro frequenza 25
fig6 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt6 = [ minMicr : stepMicr : maxMicr ]
xticks('manual')
xticks(xt6)
legend('off')
title('Misura di 25 cilindretti con micrometro normalizzati per altezza')
yl = ylabel('p^*_j');
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del micrometro
histMicr = histogram(Misure_micrometro, dataHistMicr,'Normalization','probability','FaceColor','g');

hleg = legend([histMicr], 'Micrometro');

% Converti i plot per latex
matlab2tikz('hist6.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% calibro frequenza 324
fig7 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt7 = [ minHistTotCalib : stepTotCalib : maxHistTotCalib ]
xticks('manual')
xticks(xt7)
legend('off')
title('Misura di 324 cilindretti con il calibro normalizzati per altezza')
yl = ylabel('p^*_j');
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotMicrro
histTotCalib = histogram(tot_calib, dataHistTotCalib,'Normalization','probability','FaceColor','r');

hleg = legend([histTotCalib], 'Calibro');

% Converti i plot per latex
matlab2tikz('hist7.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% micrometro frequenza 324
fig8 = figure() % crea una nuova figura
% figure(fig1) % sovrapponi a fig1
hold on
% ax = gca;
xt8 = [ minHistTotMicr : stepTotMicr : maxHistTotMicr ]
xticks('manual')
xticks(xt8)
legend('off')
title('Misura di 324 cilindretti con il micrometro normalizzati per altezza')
yl = ylabel('p^*_j');
set(yl, 'FontSize', 24);

% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
grid on;
% hold on
% set(gca,'FontSize',10)
% ax.XTickMode = 'manual';
% ax.XTick = xt1;
% ax.Color = 'blue';

% Disegno l'istogramma del TotMicrro
histTotMicr = histogram(tot_micr, dataHistTotMicr,'Normalization','probability','FaceColor','[1 0.6 0]');

hleg = legend([histTotMicr], 'Micrometro');

% Converti i plot per latex
matlab2tikz('hist8.tex','height','\figureheight', 'width','\figurewidth');
hold off

%% Calibro 324 quantili
fig9 = figure()

noCumTotCalib = cumsum(nTotCalib1) / NTotCalib1;
hCumTotCalib = bar(dataHistTotCalib1(1:end-1), noCumTotCalib, 'histc', 'BarWidth', 1);
axs = gca;
set(axs, 'NextPlot', 'add');
xlim = get(axs, 'xlim');
quant_1 = 0.1;
idx_quant = find(noCumTotCalib >= quant_1);
X_quant = dataHistTotCalib1(min(idx_quant)) + stepTotCalib1;
leg_quant1 = sprintf('%0.5g quantile = %0.5g %s', quant_1, X_quant, units);
plot([X_quant X_quant], [0 quant_1], 'r', 'linewidth', 2);
h_quant_1 = plot([xlim(1) X_quant], [quant_1 quant_1], 'r', 'linewidth', 2);
quant_2 = 0.9;
idx_quant = find(noCumTotCalib >= quant_2);
X_quant = dataHistTotCalib1(min(idx_quant)) + stepTotCalib1;
leg_quant2 = sprintf('%0.5g quantile = %0.5g %s', quant_2, X_quant, units);
plot([X_quant X_quant], [0 quant_2], 'r', 'linewidth', 2);
h_quant_2 = plot([xlim(1) X_quant], [quant_2 quant_2], 'r', 'linewidth', 2);
legend([hCumTotCalib, h_quant_1 h_quant_2], 'Dati calibro', leg_quant1, leg_quant2)
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
yl = ylabel(['P']);
set(yl, 'FontSize', 24);
tl = title('Misure lunghezza');
set(tl, 'fontsize', 24);

% Converti i plot per latex
matlab2tikz('hist9.tex','height','\bigfigureheight', 'width','\bigfigurewidth');

%% Calibro 324 micrometro
fig10 = figure()

noCumTotMicr = cumsum(nTotMicr1) / NTotMicr1;
hCumTotMicr = bar(dataHistTotMicr1(1:end-1), noCumTotMicr, 'histc', 'BarWidth', 1);
axs = gca;
set(axs, 'NextPlot', 'add');
xlim = get(axs, 'xlim');
quant_1 = 0.1;
idx_quant = find(noCumTotMicr >= quant_1);
X_quant = dataHistTotMicr1(min(idx_quant)) + stepTotMicr1;
leg_quant1 = sprintf('%0.5g quantile = %0.5g %s', quant_1, X_quant, units);
plot([X_quant X_quant], [0 quant_1], 'r', 'linewidth', 2);
h_quant_1 = plot([xlim(1) X_quant], [quant_1 quant_1], 'r', 'linewidth', 2);
quant_2 = 0.8;
idx_quant = find(noCumTotMicr >= quant_2);
X_quant = dataHistTotMicr1(min(idx_quant)) + stepTotMicr1;
leg_quant2 = sprintf('%0.5g quantile = %0.5g %s', quant_2, X_quant, units);
plot([X_quant X_quant], [0 quant_2], 'r', 'linewidth', 2);
h_quant_2 = plot([xlim(1) X_quant], [quant_2 quant_2], 'r', 'linewidth', 2);
quant_3 = 0.9;
idx_quant = find(noCumTotMicr >= quant_3);
X_quant = dataHistTotMicr1(min(idx_quant)) + stepTotMicr1;
leg_quant3 = sprintf('%0.5g quantile = %0.5g %s', quant_3, X_quant, units);
plot([X_quant X_quant], [0 quant_3], 'r', 'linewidth', 2);
h_quant_3 = plot([xlim(1) X_quant], [quant_3 quant_3], 'r', 'linewidth', 2);
legend([hCumTotMicr, h_quant_1 h_quant_2, h_quant_3], 'Dati micrometro', leg_quant1, leg_quant2, leg_quant3,'location','northwest')
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);
yl = ylabel(['P']);
set(yl, 'FontSize', 24);
tl = title('Misure lunghezza');
set(tl, 'fontsize', 24);

% Converti i plot per latex
matlab2tikz('hist10.tex','height','\figureheight', 'width','\figurewidth');