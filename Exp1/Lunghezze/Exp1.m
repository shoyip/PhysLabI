% Chiudo tutte le figure aperte
close all

% Scelgo le unita' di misura
units = 'mm';


%% Importazione dei dati micrometro
% Ipotizzo di aver creato (in qualche modo, ad esempio con il tool per
% l'importazione dei dati) una funzione chiamata import_cylinders_file.m
% che prende i dati dalla seconda colonna di un file
%
% Misure_micrometro = import_cylinders_file('Misure_micrometro.csv');

[Numero_campione,Misure_calibro,Misure_micrometro,Sottile] = importfile('misure1.csv',2, 26);

%% Analisi dei dati: micrometro
% A questo punto posso assumere di avere gia' caricato i dati, 
% mettendoli in una variabile, chiamata Misure_micrometro

% Trovo minimo e massimo delle misure per calcolare l'intervallo di incertezza massima.

Misure_micrometro

Misure_micrometro_min = min(Misure_micrometro)
Misure_micrometro_max = max(Misure_micrometro)

% Risoluzione dello strumento: micrometro
deltaX_micrometro = 0.01;

% Definizione del vettore dei bins: micrometro
x_micrometro_step = 8*deltaX_micrometro;
x_micrometro_min  = Misure_micrometro_min - x_micrometro_step/2;
%x_micrometro_min  = Misure_micrometro_min;
x_micrometro_max  = Misure_micrometro_max + x_micrometro_step/2;
X_micrometro = x_micrometro_min : x_micrometro_step : x_micrometro_max;

%% Calcolo dei conteggi, dati micrometro:
n_micrometro = histcounts(Misure_micrometro, X_micrometro);

%% Preparo un report
disp(' ');
disp('#### Dati micrometro:');

%% Dati micrometro: media campionaria
mean_m = mean(Misure_micrometro);

display_string = sprintf('Media: %f %s\n', mean_m, units);
disp(display_string);
% Scelgo le unita' di misura
units = 'mm';

%% Dati micrometro: varianza campionaria
% Uso la definizione con 1/N
var_m = var(Misure_micrometro, 1);
fprintf('Varianza campionaria: %f %s^2\n', var_m, units);

%% Dati micrometro: deviazione standard campionaria
% Uso la definizione con 1/N
std_m = std(Misure_micrometro, 1);
fprintf('Scarto quadratico medio campionario: %f %s\n', std_m, units);

%% Dati micrometro: mediana campionaria
median_m = median(Misure_micrometro);
fprintf('Mediana: %f %s\n', median_m, units);

%% Calcolo il numero totale di campioni
N_micrometro = sum(n_micrometro);

%% Dati micrometro: frequenza campionaria:
p_micrometro = n_micrometro / N_micrometro;

% Verifico:
fprintf('Somma dei conteggi: %d\n', N_micrometro);
fprintf('Somma delle frequenze campionarie: %d\n', sum(p_micrometro));

%% Calcolo densita' campionaria:
f_micrometro = p_micrometro / x_micrometro_step;

% Verifico:
fprintf('Somma densita'' campionarie: %d\n', sum(f_micrometro .* x_micrometro_step));
disp(' ');


%% Importazione dei dati micrometro
% Ipotizzo di aver creato (in qualche modo, ad esempio con il tool per
% l'importazione dei dati) una funzione chiamata import_cylinders_file.m
% che prende i dati dalla seconda colonna di un file
%
% Misure_calibro = import_cylinders_file('Misure_calibro.csv');

%% Analisi dei dati: calibro
% A questo punto posso assumere di avere gia' caricato i dati, 
% mettendoli in una variabile, chiamata Misure_calibro
Misure_calibro
min(Misure_calibro)
max(Misure_calibro)

% Risoluzione dello strumento: calibro
deltaX_calibro = 0.05;

% Definizione del vettore dei bins: calibro
x_calibro_step = 2*deltaX_calibro;
x_calibro_min  = min(Misure_calibro) - x_calibro_step/2;
x_calibro_max  = max(Misure_calibro) + x_calibro_step/2;
X_calibro = x_calibro_min : x_calibro_step : x_calibro_max;

%% Calcolo dei conteggi, dati calibro:
n_calibro = histcounts(Misure_calibro, X_calibro);

%% Preparo un report
disp(' ');
disp('#### Dati calibro:');

%% Dati calibro: media campionaria
mean_m = mean(Misure_calibro);
fprintf('Media: %f %s\n', mean_m, units);

%% Dati calibro: varianza campionaria
% Uso la definizione con 1/N
var_m = var(Misure_calibro, 1);
fprintf('Varianza campionaria: %f %s\n', var_m, units);

%% Dati calibro: deviazione standard campionaria
% Uso la definizione con 1/N
std_m = std(Misure_calibro, 1);
fprintf('Scarto quadratico medio campionario: %f %s\n', std_m, units);

%% Dati calibro: mediana campionaria
median_m = median(Misure_calibro);
fprintf('Mediana: %f %s\n', median_m, units);

%% Calcolo il numero totale di campioni
N_calibro = sum(n_calibro);

%% Dati calibro: frequenza campionaria:
p_calibro = n_calibro / N_calibro;

% Verifico:
fprintf('Somma dei conteggi: %d\n', N_calibro);
fprintf('Somma delle frequenze campionarie: %d\n', sum(p_calibro));

%% Calcolo densita' campionaria:
f_calibro = p_calibro / x_calibro_step;

% Verifico:
fprintf('Somma densita'' campionarie: %d\n', sum(f_calibro .* x_calibro_step));
disp(' ');

%% Rappresentazione grafica: nuova figura
% Apre nuova finestra per grafico
fig1 = figure();

xticks(x_micrometro_min + x_micrometro_step/2 : x_micrometro_step : x_micrometro_max - x_micrometro_step/2 );

% Rappresentazione grafica: istogramma misure micrometro
h_micrometro = histogram(Misure_micrometro, X_micrometro);

%% Accediamo agli assi
axs1 = gca;

% Scelta della colorazione degli istogrammi:
% true    usa i colori
% false   non usa i colori
hist_color_full = true;

%% Personalizzazione: colore colonne
if hist_color_full
  set(h_micrometro, 'FaceColor', [0 0 1]);    % Blu, specificabile anche con 'b'
else
  set(h_micrometro, 'FaceColor', [1 1 1]);    % Bianco
end

%% Personalizzazione: trasparenza colonne
set(h_micrometro, 'FaceAlpha', 0.5);

%% Personalizzazione: colore bordi
set(h_micrometro, 'EdgeColor', [0 0 1]);

%% Specifico piu' plot negli stessi assi
hold on

%% Oppure, meglio: specifico piu' plot negli stessi assi
set(axs1, 'NextPlot', 'add');

%% Personalizzazione: label y
yl = ylabel('Counts');
set(yl, 'FontSize', 24);

%% Personalizzazione: label x
% Uso la sintassi per concatenare orizzontalmente stringhe
xl = xlabel(['L [' units ']']);
set(xl, 'FontSize', 24);

%% Personalizzazione: titolo
tl = title('Misure lunghezza');
set(tl, 'fontsize', 24);

%% Plot yy

[ax, h_left, h_right] = plotyy

%% Specifico la larghezza delle barre
set(h_right, 'BarWidth', 1);
set(h_left, 'BarWidth', 1);