%% Secondo report lunghezze

[Numero_campione,Calib25,Micro25,Sottile] = importfile('misure1.csv',2, 26);

% calib25
mCalib25 = mean(Calib25)
stdCalib25 = std(Calib25)
varCalib25 = var(Calib25, 1)
medCalib25 = median(Calib25)
q10Calib25 = quantile(Calib25, .1)
q90Calib25 = quantile(Calib25, .9)

% micro25
mMicro25 = mean(Micro25)
stdMicro25 = std(Micro25)
varMicro25 = var(Micro25, 1)
medMicro25 = median(Micro25)
q10Micro25 = quantile(Micro25, .1)
q90Micro25 = quantile(Micro25, .9)

[tot_no,Calib324,Micro324] = importfile1('dati.csv',2, 325);

% calib324
mCalib324 = mean(Calib324)
stdCalib324 = std(Calib324, 1)
varCalib324 = var(Calib324, 1)
medCalib324 = median(Calib324)
q10Calib324 = quantile(Calib324, .1)
q90Calib324 = quantile(Calib324, .9)

% micro324
mMicro324 = mean(Micro324)
stdMicro324 = std(Micro324)
varMicro324 = var(Micro324, 1)
medMicro324 = median(Micro324)
q10Micro324 = quantile(Micro324, .1)
q90Micro324 = quantile(Micro324, .9)

M = [%'Misura' '$\m^*\[x_i\] (\SI{\milli\meter})$' '$\sigma^*\[x_i\] (\SI{\milli\meter})$' '$D^*\[x_i\] (\SI{\milli\meter^2})$' 'Mediana ($\SI{\milli\meter}$)' '$q_{10} (\SI{\milli\meter})$' 'q_{90} (\SI{\milli\meter})';
        mCalib25 stdCalib25 varCalib25 medCalib25 q10Calib25 q90Calib25;
        mMicro25 stdMicro25 varMicro25 medMicro25 q10Micro25 q90Micro25;
        mCalib324 stdCalib324 varCalib324 medCalib324 q10Calib324 q90Calib324;
        mMicro324 stdMicro324 varMicro324 medMicro324 q10Micro324 q90Micro324]