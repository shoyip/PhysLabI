\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Introduzione}{2}
\contentsline {section}{\numberline {2}Misura delle lunghezze}{2}
\contentsline {subsection}{\numberline {2.1}Materiali e metodi}{2}
\contentsline {subsection}{\numberline {2.2}Risultati e analisi dati}{2}
\contentsline {subsection}{\numberline {2.3}Discussione}{3}
\contentsline {section}{\numberline {3}Misura del periodo del pendolo}{4}
\contentsline {subsection}{\numberline {3.1}Materiali e metodi}{4}
\contentsline {subsection}{\numberline {3.2}Risultati e analisi dati}{5}
\contentsline {subsection}{\numberline {3.3}Discussione}{6}
\contentsline {section}{\numberline {4}Conclusioni}{7}
