close all

% Import dei file con le misure
[t0_4,t0_5,t0_6,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t0] = importfile('dati_osc.csv',2, 11);

[m1,m2] = importfile1('dati_masse.csv',2, 12);

[z0,z1,z2,z3,z4,z5,z6,z7,z8,z9,z10] = importfile2('dati_tot.csv',2, 11);

% Accelerazione di gravita'
g = 9.806;  % m s^-2
m = [m1; m2];
% Matrice con le deformazioni in colonna
B = [z0 z1 z2 z3 z4 z5 z6 z7 z8 z9 z10];
A = B.*10^(-2);

% Risoluzione masse applicate 
masse_applicate = 1e-3 * m1; %kg

% Risoluzione della bilancia
Ris_bilancia = 1e-4; % kg

% Incertezza di risoluzione bilancia
dRis_bilancia = Ris_bilancia / sqrt(12);

% Calcolo i pesi applicati e la loro incertezza
P  = masse_applicate * g;  % N
dP = dRis_bilancia * g * ones(size(m2)); % N

P_units = 'N';
x_units = 'm';

%% PRIMA PARTE
% Vettore delle deformazione
media_x= mean(A).';
% Adesso le incertezze...
% Risoluzione del metro a nastro
Dx = 5e-4; % mD
% Incertezza dovuta alla risoluzione e alla varianza campionaria
dx = sqrt((Dx^2 / 6 * ones(size(media_x))) + var(A).');

%% Vettore deformazione (z0 - zi)
Def_1 = (mean(A(:,1)) .* ones(size(media_x))) - media_x;
Def_1(1) = 0;
% Incertezza dovuta alla risoluzione alla varianza sulla misura di z0 e alle
% misure successive.
dDef_1 = sqrt(Dx^2 / 6 .* ones(size(media_x)) + var(A(:,1)) .* ones(size(media_x)) + var(A).');
dDef_1(1) = Dx/sqrt(12);

%% Stime della costante elastica
k = P ./ Def_1;
dk = k .* sqrt((dDef_1./media_x).^2 + (dP./P).^2); % E l'errore relativo sulla misura nulla?

%% Calcolo dei pesi statistici, come:
% inverso del quadrato dell'indeterminazione delle misure di deformazione
w = 1./(dk.^2);

%% Stima della costante elastica: media pesata dei rapporti forza/deformazione
% Incertezza nella stima della costante elastica: propagazione dell'errore
[k0, dk0] = media_pesata(k, w);

%% Plot con barre d'errore sulle ordinate
fig = figure();
hold on
grid on
he = errorbar(P, Def_1, dDef_1);

% Non traccio le linee di congiunzione
set(he, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(he, 'marker', '.', 'markersize', 16);
% Le barre d'errore le faccio magenta
set(he, 'color', 'm')
% E i punti li coloro di nero
set(he, 'MarkerFaceColor', 'k')
set(he, 'MarkerEdgeColor', 'k')

%% Aggiusto la forma dei 'cappelli'
set(he, 'Capsize', 0)

%% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 24);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 24);

hold off
%% Plot con barre d'errore sulle ascisse e sulle ordinate
fig = figure();
h = errorbar(P, Def_1, dDef_1, dDef_1, dP, dP);

% Mi segno gli assi correnti (ci sono penso modi piu' eleganti di farlo) 
axs_all = fig.Children(1);

% Non traccio le linee di congiunzione
set(h, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(h, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(h, 'markeredgecolor', 'k')
set(h, 'markerfacecolor', 'k')

% Le barre vere e proprie le faccio magenta
set(h, 'color', 'm')

%% Aggiusto la forma dei 'cappelli'
h.CapSize = 0;

grid on
hold on


%% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 24);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 24);

%% Aggiungo uno 'zoom' attorno ad un punto
% Creo dei nuovi assi, piccoli
% pos = [left bottom width height];
% zoompos = [0.70    0.15    0.2    0.35];
zoompos = [0.68    0.235    0.2    0.35];
zoomaxs = axes('position', zoompos);

h_zoom = errorbar(P, Def_1, dDef_1, dDef_1, dP, dP);

% Non traccio le linee di congiunzione
set(h_zoom, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(h_zoom, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(h_zoom, 'markeredgecolor', 'k')
set(h_zoom, 'markerfacecolor', 'k')

% Le barre vere e proprie le faccio verdi
set(h_zoom, 'color', 'g')

% Imposto le dimensioni
set(h_zoom, 'capsize', 0)

grid on
hold on

% Finalmente, il vero e proprio zoom
% Scelgo il 5o punto della serie
idx = 5;
% Imposto il range degli assi a +/- 3 volte l'incerte   zza
% Per farlo uso due diverse sintassi
% Usando xlim/ylim
xlim(P(idx) + 3*[-dP(idx) dP(idx)])
% Oppure usando set della proprieta' xlim/ylim
set(zoomaxs, 'ylim', Def_1(idx) + 3*[-dDef_1(idx) dDef_1(idx)]);

% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 16);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 16);

%% Regressione lineare (prop diretta)
% x = b0 * P
% Implemento le formule nel testo ;)
b0 = sum(P .* Def_1) / sum(P.^2);

% Le incertezze x sono tutte uguali
db = dx(1) ./ sqrt(sum(P.^2));

%% Calcolo delle previsioni del modello
model_x = b0 * P;

%% Test del chi^2
% chi^2 = sum((y - modello).^2 ./ dy.^2) dove
% y  = dati
% dy = incertezza nei dati
Chi2 = chi2(Def_1, dx, model_x);
N = numel(Def_1);
dof = N - 1;

%% Aggiungo le previsioni del modello al grafico con le deformazioni misurate:
% Specifico l'asse giusto
p = plot(axs_all, P, model_x);
% Cambio colore
set(p, 'color', [1 0 0])
% Infine una legenda, ovviamente ...
ll = legend([h, p], 'Dati', 'Modello');
set(ll, 'FontSize', 18, 'Location', 'NorthWest');

%% Plot dei residui: abs(dati - modello)
figure()
r = plot(P, model_x - Def_1);
% Non traccio le linee di congiunzione
set(r, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(r, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(r, 'color', 'k')
% Aggiungo un titolo ed una legenda
tit = title('Residui');
leg = legend(r, 'Modello lineare, residui');

%% Incertezze 'a posteriori'
dx_p = sqrt(1/(dof) * sum((Def_1 - model_x).^2));


%% Caloclo della media pesata sui valori di k
mediaPesata = media_pesata(k, w);

%% SECONDA PARTE
% Bisogna considerare i periodi di oscillazione di dieci campioni di massa.
% Sulle ascisse abbiamo le masse, sulle ordinate i periodi.
C = [t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 t10];
meanC = mean(C);
stdC = std(C);
figure()
h2 = errorbar(m2, meanC, stdC, stdC, dRis_bilancia * ones(11,1), dRis_bilancia * ones(11,1));

% Non traccio le linee di congiunzione
set(h2, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(h2, 'marker', '.', 'markersize', 16);