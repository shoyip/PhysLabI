\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {section}{\numberline {1}Introduzione}{2}
\contentsline {section}{\numberline {2}Materiali e strumenti di misura}{2}
\contentsline {section}{\numberline {3}Misura del $k$ statico}{2}
\contentsline {subsection}{\numberline {3.1}Procedimento}{2}
\contentsline {subsection}{\numberline {3.2}Risultati e analisi dati}{3}
\contentsline {subsection}{\numberline {3.3}Discussione}{9}
\contentsline {subsubsection}{\numberline {3.3.1}Scelta della media}{9}
\contentsline {subsubsection}{\numberline {3.3.2}Carico e scarico molla}{9}
\contentsline {section}{\numberline {4}Misura del $k$ dinamico}{9}
\contentsline {subsection}{\numberline {4.1}Procedimento}{9}
\contentsline {subsection}{\numberline {4.2}Risultati e analisi dati}{9}
\contentsline {subsection}{\numberline {4.3}Discussione}{12}
\contentsline {subsubsection}{\numberline {4.3.1}Calcolo di $C^2$ e massa efficace}{12}
\contentsline {section}{\numberline {5}Conclusioni}{12}
