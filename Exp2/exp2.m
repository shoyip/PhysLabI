% Importo le misure singole
[t0_1,t0_2,t0_3,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t0] = importfile('dati_osc.csv',2, 11);
% importo le masse applicate
 [m1,m2] = importfile1('dati_masse.csv',2, 12);
% Importo gli allungamenti 
[za0,za1,za2,za3,za4,za5,za6,za7,za8,za9,za10,zb0,zb1,zb2,zb3,zb4,zb5,zb6,zb7,zb8,zb9,zb10] = importfile2('dati_tot.csv',2, 6);
[zia,zib] = importfile3('dati_plus.csv',2, 12);

% Calcola la deviazione standard di ogni operatore
std0_1 = std(t0_1);
std0_2 = std(t0_2);
std0_3 = std(t0_3);

std0_1
std0_2
std0_3

A = [za0,za1,za2,za3,za4,za5,za6,za7,za8,za9,za10]
B = [zb0,zb1,zb2,zb3,zb4,zb5,zb6,zb7,zb8,zb9,zb10]
mean(std(A))
mean(std(B))
%%
% Accelerazione di gravita'
g = 9.806;  % m s^-2

% Vettore delle masse applicate
m = 1e-3 .* m2; % kg

% Risoluzione della bilancia
Dm = 1e-4; % kg

% Incertezza di risoluzione
dm = Dm / sqrt(12);

% Calcolo i pesi applicati e la loro incertezza
P  = m * g;  % N
dP = dm * g * ones(size(m)); % N

P_units = 'N';
x_units = 'm';

%% Vettore delle deformazioni
x = mean(A); % m
%% Incertezza nelle deformazioni
% Risoluzione del metro a nastro
Dx = 1e-3; % m

% Mi servira' un vettore delle dimensioni giuste
%dx = Dx / sqrt(6) * ones(size(x));
dx = sqrt(var(A) + Dx / sqrt(6) * ones(size(x)))

%% Stime della costante elastica
k = P ./ x;

%% Stime dell'incertezza della costante elastica
dk = k .* sqrt((dx./x).^2 + (dP./P).^2);

%% Calcolo dei pesi statistici, come:
% inverso del quadrato dell'indeterminazione delle misure di deformazione
w = 1./(dk.^2);

%% Stima della costante elastica: media pesata dei rapporti forza/deformazione
% Incertezza nella stima della costante elastica: propagazione dell'errore
[k0, dk0] = media_pesata(k, w);

%% Plot con barre d'errore sulle ordinate
fig = figure();
he = errorbar(P, x, dx);

% Non traccio le linee di congiunzione
set(he, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(he, 'marker', '.', 'markersize', 16);
% Le barre d'errore le faccio magenta
set(he, 'color', 'm')
% E i punti li coloro di nero
set(he, 'MarkerFaceColor', 'k')
set(he, 'MarkerEdgeColor', 'k')
  
%% Aggiusto la forma dei 'cappelli'
set(he, 'Capsize', 0)

%% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 24);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 24);


%% Plot con barre d'errore sulle ascisse e sulle ordinate
fig = figure();
h = errorbar(P, x, dx, dx, dP, dP);

% Mi segno gli assi correnti (ci sono penso modi piu' eleganti di farlo) 
axs_all = fig.Children(1);

% Non traccio le linee di congiunzione
set(h, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(h, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(h, 'markeredgecolor', 'k')
set(h, 'markerfacecolor', 'k')

% Le barre vere e proprie le faccio magenta
set(h, 'color', 'm')


%% Aggiusto la forma dei 'cappelli'
h.CapSize = 0;

grid on
hold on

%% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 24);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 24);

%% Aggiungo uno 'zoom' attorno ad un punto
% Creo dei nuovi assi, piccoli
% pos = [left bottom width height];
% zoompos = [0.70    0.15    0.2    0.35];
zoompos = [0.68    0.235    0.2    0.35];
zoomaxs = axes('position', zoompos);

h_zoom = errorbar(P, x, dx, dx, dP, dP);

% Non traccio le linee di congiunzione
set(h_zoom, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(h_zoom, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(h_zoom, 'markeredgecolor', 'k')
set(h_zoom, 'markerfacecolor', 'k')

% Le barre vere e proprie le faccio verdi
set(h_zoom, 'color', 'g')

% Imposto le dimensioni
set(h_zoom, 'capsize', 0)

grid on
hold on

% Finalmente, il vero e proprio zoom
% Scelgo il 5o punto della serie
idx = 5;
% Imposto il range degli assi a +/- 3 volte l'incertezza
% Per farlo uso due diverse sintassi
% Usando xlim/ylim
xlim(P(idx) + 3*[-dP(idx) dP(idx)])
% Oppure usando set della proprieta' xlim/ylim
set(zoomaxs, 'ylim', x(idx) + 3*[-dx(idx) dx(idx)]);

% Imposto le etichette con le unita' di misura
xl = xlabel(['P [' P_units ']']);
set(xl, 'FontSize', 16);

yl = ylabel(['\Deltax [' x_units ']']);
set(yl, 'FontSize', 16);

%% 3) regressione lineare (prop diretta)
% x = b0 * P
% Implemento le formule nel testo ;)
b0 = sum(P .* x) / sum(P.^2);

% Le incertezze x sono tutte uguali
db = dx(1) ./ sqrt(sum(P.^2));
%% 4) Calcolo delle previsioni del modello
model_x = b0 * P;

%% 5) Test del chi^2
% chi^2 = sum((y - modello).^2 ./ dy.^2) dove
% y  = dati
% dy = incertezza nei dati
Chi2 = chi2(x, dx, model_x);
N = numel(x);
dof = N - 1;

%% 6) Aggiungo le previsioni del modello al grafico con le deformazioni misurate:
% Specifico l'asse giusto
p = plot(axs_all, P, model_x);
% Cambio colore
set(p, 'color', [1 0 0])
% Infine una legenda, ovviamente ...
ll = legend([h, p], 'Dati', 'Modello');
set(ll, 'FontSize', 18, 'Location', 'NorthWest');


%% 6) Plot dei residui: abs(dati - modello)
figure()
r = plot(P, model_x - x);
% Non traccio le linee di congiunzione
set(r, 'linestyle', 'none')
% Visualizzo invece i punti singoli
set(r, 'marker', '.', 'markersize', 16);
% E li coloro di nero
set(r, 'color', 'k')
% Aggiungo un titolo ed una legenda
tit = title('Residui');
leg = legend(r, 'Modello lineare, residui');

% Qui 

%% 7) Incertezze 'a posteriori'
dx_p = sqrt(1/(dof) * sum((x - model_x).^2));

