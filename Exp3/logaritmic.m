%% Esempio di analisi dati 
% per l'esperimento del pendolo semplice: 
% grafici logaritmici
% 
% M Hueller 03/2013
%
% $Id: logaritmic.m 6021 2018-04-09 08:17:58Z mauro.hueller $

%% Preparo dati di prova
f = [1:1:100]';
df = ones(size(f));

V1 = 5 ./ f.^1;
V2 = f.^2;
V3 = 10 ./ f.^3;
dV = 0.1*ones(size(f));


%% Grafico logaritmico diretto
figure;
ll1 = loglog(f, V1);
hold on;
grid on;
ll2 = loglog(f, V2);
ll3 = loglog(f, V3);

% Personalizzo i colori
set(ll1, 'color', 'b');
set(ll2, 'color', 'r');
set(ll3, 'color', 'k');

% Aggiungo la legenda
leg = legend([ll1;ll2;ll3], {'f^{-1}', 'f^{2}', 'f^{-3}'});

% Descrizione assi
xl = xlabel('Frequency [Hz]');
yl = ylabel('Output [V]');


%% Grafico logaritmico indiretto
fff = figure;
axs = axes();
ll1 = plot(f, V1);
hold on;
grid on;
ll2 = plot(f, V2);
ll3 = plot(f, V3);

% Specifico gli assi: logaritmici
set(axs, 'YScale', 'log');
set(axs, 'XScale', 'log');
set(axs, 'fontsize', 12);
set(axs, 'fontname', 'Lucida Grande');
set(axs, 'XLim', [1 1000]);
set(axs, 'YLim', [1e-5 1e4]);

% Personalizzo i colori
set(ll1, 'color', 'b');
set(ll2, 'color', 'r');
set(ll3, 'color', 'k');

% Aggiungo la legenda
leg = legend([ll1;ll2;ll3], {'f^{-1}', 'f^{2}', 'f^{-3}'});

% Descrizione assi
xlabel('Frequency [Hz]');
ylabel('Output [V]');

%% Grafico logaritmico indiretto
figure;
axs = axes();
ll1 = errorbar(f, V1, dV);
hold on;
grid on;

% Specifico gli assi: logaritmici
set(axs, 'YScale', 'log');
set(axs, 'XScale', 'log');

% Personalizzo i colori
set(ll1, 'color', 'b');

% Aggiungo la legenda
leg = legend([ll1;], {'f^{-1}'});

% Descrizione assi
xlabel('Frequency [Hz]');
ylabel('Output [V]');

%% Grafico lineare del logaritmo:
Lf  = log10(f);
LV1 = log10(V1);
LV2 = log10(V2);
LV3 = log10(V3);

figure;
axs = axes();
ll1 = plot(Lf, LV1);
hold on;
grid on;
ll2 = plot(Lf, LV2);
ll3 = plot(Lf, LV3);

% Specifico gli assi: lineari
set(axs, 'YScale', 'linear');
set(axs, 'XScale', 'linear');

% Personalizzo i colori
set(ll1, 'color', 'b');
set(ll2, 'color', 'r');
set(ll3, 'color', 'k');

% Aggiungo la legenda
leg = legend([ll1;ll2;ll3], {'f^{-1}', 'f^{2}', 'f^{-3}'});

% Descrizione assi
xlabel('log10(Frequency [Hz])');
ylabel('log10(Output [V])');


