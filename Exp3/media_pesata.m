function [m, dm] = media_pesata(dati, pesi)
  % function [m, dm] = media_pesata(dati, pesi) 
  % Calcola la media pesata del vettore dati, pesata sul vettore pesi:
  %         m  = sum(dati .* pesi) ./ sum(pesi);
  % E l'incertezza: 
  %         dm = sqrt(1 ./ sum(pesi));
  %
  % M Hueller 19/02/2014
  %
  % $Id: media_pesata.m 5965 2017-03-20 05:13:09Z mauro.hueller $
  
  % Verifico che il vettore pesi non sia vuoto
  if ~isempty(pesi)
    % Controllo le dimensioni del vettore pesi
    if ~isequal(size(dati), size(pesi))
      error('Vettore pesi e vettore dati devono avere le stesse dimensioni!')
    else
      % Tutto bene
      m  = sum(dati .* pesi) ./ sum(pesi);
      dm = sqrt(1 ./ sum(pesi));
    end
  else
    % Nel caso il vettore pesi sia vuoto, ritorno la media artimetica
    warning('Vettore pesi vuoto. Calcolo la media aritmetica! L''incertezza e'' la deviazione standard della media.');
    m  = mean(dati);
    dm = sqrt(1/numel(dati)) * std(dati);
  end
end