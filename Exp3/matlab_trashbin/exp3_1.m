%% Script della terza esperienza: periodo del pendolo
close all

mUnits = 'g';
lUnits = 'm';
tUnits = 's';

g = 9.8071;

[hCil,mCil,lFilo] = importfile('masse.csv',2, 5);
[T1_3Vale,T1_3Pego,T1_3Sho,T1_1,T1_2,T1_4] = importfile1('misure3_1.csv',2, 6);
lFilo2 = importfile2('lunghezze.csv',2, 11);
[T2_1,T2_2,T2_3,T2_4,T2_5,T2_6,T2_7,T2_8,T2_9,T2_10] = importfile3('misure3_2.csv',2, 6);

%% Misura 1: Dipendenza dalla massa

masse = mCil;
DWeight = 0.1;
dWeight = DWeight / sqrt(12);
dMasse = dWeight * ones(4,1);
% Il filo è misurato con il flessometro
DFlex = 1e-2;
dFlex = DFlex / sqrt(12);
% Il cilindro è misurato con il calibro a nonio
DCalib = 5e-5;
dCalib = DCalib / sqrt(12);

% Calcolo valore più attendibile e errori
T1_3 = T1_3Pego;
T1 = [T1_1 T1_2 T1_3 T1_4];
meanT1 = mean(T1);
varT1 = var(T1);
stdT1 = std(T1);
dT1 = stdT1;

% Faccio un report dei parametri
fprintf("\n=== REPORT DELLE MASSE ===\n")
for ii=1:4
    fprintf("La massa %i ha media %.2f %s varianza %.3f %s^2 e deviazione standard %.2f %s.\n", ii, meanT1(ii), tUnits, varT1(ii), tUnits, stdT1(ii), tUnits);
end
fprintf("\n");

masse = masse.';
dMasse = dMasse.';
p = polyfit(masse, meanT1, 0);
fitT1 = polyval(p,masse);

% Calcolo il valore atteso del periodo dalla formula
Tatt = 2 * pi * sqrt((mean(lFilo) + hCil(1)/2) / g) * 10;

% Calcolo la media pesata dei periodi e verifico la compatibilità con il
% test del chi2
w = 1 ./ stdT1.^2;
mPesPeriodi = media_pesata(meanT1, w);
chi2Periodi = chi2(meanT1, dT1, Tatt);

%% Figura
% Trova retta massima, minima e media pendenza
% In ascissa ci sono le masse, in ordinata i periodi
fig1 = figure();
hold on
hT1 = errorbar(masse, meanT1, dT1, dT1, dMasse, dMasse);
hT1.CapSize = 0;
set(hT1, 'marker', '.', 'markersize', 20);
set(hT1, 'linestyle', 'none');
%maxMaxErrT1 = max(meanT1 + dT1);
%minMaxErrT1 = min(meanT1 + dT1);
%maxMinErrT1 = max(meanT1 - dT1);
%minMinErrT1 = min(meanT1 - dT1);
%plot([masse(2), masse(4)], [meanT1(1) - dT1(1), meanT1(4) + dT1(4)]);
%plot([masse(1), masse(3)], [meanT1(1) + dT1(1), meanT1(4) - dT1(4)]);
%m1 = Tatt * ones(1, length(mCil));
plot(mCil, m1);
plot(masse,fitT1,'-');
grid on
hold off

%% Dipendenza dalla lunghezza

lTot = lFilo + (hCil / 2);